A small micro kernel which does nothings. For now only the interrupt and basic
io are handled. On dev, the pagination is almost done but thre is still some
bugs.

# Dependencies
    * To build the iso: genisoimage
    * (Optional) To run: bochs-sdl

# Usage:
    * `make` to build the iso.
    * `make run` to build and run using bochs.

# Ascii art by:
    * https://ascii.co.uk/art/corn
    * http://patorjk.com/software/taag
