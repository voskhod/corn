#include "serial.h"
#include "wrapper.h"

#define SERIAL_DATA_PORT(base) (base)
#define SERIAL_FIFO_CMD_PORT(base) (base + 2)
#define SERIAL_LINE_CMD_PORT(base) (base + 3)
#define SERIAL_MODEM_CMD_PORT(base) (base + 4)
#define SERIAL_LINE_STATUS_PORT(base) (base + 5)

#define SERIAL_ACTIVATE_DLAB 0x80


static void configure_baud_rate(u16 com, u16 diviseur);
static int is_transit_empty(u16 com);


void serial_init(u16 com)
{
    /* Divide the serial speed by 3 */
    configure_baud_rate(com, 3);

    /* 8 bits, no parity, one stop bit */
    outb(SERIAL_LINE_CMD_PORT(com), 0x03);

    /* Enable FIFO, clear them, with a size of 14 bytes */
    outb(SERIAL_FIFO_CMD_PORT(com), 0xC7);

    /* No interrupt, RTS = 1 and DTR = 1 */
    outb(SERIAL_MODEM_CMD_PORT(com), 0x03);
}


void serial_write(u16 com, char c)
{
    while (!is_transit_empty(com))
        continue;

    outb(com, c);
}


static int is_transit_empty(u16 com)
{
    return inb(SERIAL_LINE_STATUS_PORT(com)) & 0x20;
}

static void configure_baud_rate(u16 com, u16 divisor)
{
    outb(SERIAL_LINE_CMD_PORT(com), SERIAL_ACTIVATE_DLAB);
    outb(SERIAL_DATA_PORT(com), (divisor >> 8) & 0x00FF);
    outb(SERIAL_DATA_PORT(com), divisor & 0x00FF);
}
