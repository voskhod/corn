#include "io.h"
#include "wrapper.h"

struct frame
{
    /* The character to print */
    char c;
    /* The first 4 bits are the foreground and the 4 lasts the background */
    u8 color;
} __attribute__((packed));


/* The frames buffer */
static struct frame *f_buffer = (struct frame *)0x000B8000;
/* The pos of cursor, should alway be between 0 and IO_ROWS * IO_COLUMNS */
static u16 io_cursor_pos = 0;


static void scroll();
static inline unsigned short get_line_begining(u16 pos);
static void add_new_line();


void io_clear()
{
    for (unsigned i = 0; i < IO_ROWS * IO_COLUMNS; i++)
        ((short*)f_buffer)[i] = 0;
}


int io_write(const char *buffer, unsigned n)
{
    if (buffer == NULL)
        return 0;

    for (unsigned int i = 0; i < n; i++)
        io_write_char(buffer[i]);

    return n;
}


void io_write_char(char c)
{
    if (c == '\n')
        add_new_line();
    else
    {
        io_write_frame(io_cursor_pos, c, IO_WHITE, IO_BLACK);
        io_move_cursor(io_cursor_pos + 1);
    }
}

int io_print(const char *buffer)
{
    if (buffer == NULL)
        return 0;

    int i = 0;
    for (; buffer[i] != '\0'; i++)
        io_write_char(buffer[i]);

    return i;

}


void io_write_frame(u16 pos, char c, u8 bg_color, u8 fg_color)
{
    f_buffer[pos].c = c;
    f_buffer[pos].color = ((fg_color & 0x0F) << 4) | (bg_color & 0x0F);
}


void io_move_cursor(u16 pos)
{
    io_cursor_pos = pos;
    if (io_cursor_pos >= IO_ROWS * IO_COLUMNS)
        scroll();

    outb(IO_PORT_COMMAND, IO_COMMAND_BYTE_SUP);
    outb(IO_PORT_DATA, (pos >> 8) & 0xFF);

    outb(IO_PORT_COMMAND, IO_COMMAND_BYTE_INF);
    outb(IO_PORT_DATA, pos & 0xFF);
}


static void add_new_line()
{
    io_cursor_pos = get_line_begining(io_cursor_pos) + 80;

    if (io_cursor_pos >= IO_ROWS * IO_COLUMNS)
        scroll();
    else
        io_move_cursor(io_cursor_pos);
}


void scroll()
{
    for (unsigned short i = IO_COLUMNS; i < IO_COLUMNS * IO_ROWS; i++)
        ((short *)f_buffer)[i - IO_COLUMNS] = ((short *)f_buffer)[i];

    if (io_cursor_pos > IO_COLUMNS)
        io_cursor_pos = get_line_begining(io_cursor_pos) - IO_COLUMNS;
    else
        io_cursor_pos = 0;

    io_move_cursor(io_cursor_pos);
}


static inline unsigned short get_line_begining(u16 pos)
{
    return pos - (pos % IO_COLUMNS);
}

