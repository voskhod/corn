#ifndef IO_H
#define IO_H

#include "types.h"

#define IO_COLUMNS 80
#define IO_ROWS 25

/* Colors */
#define IO_BLACK 0
#define IO_BLUE 1
#define IO_GREEN 2
#define IO_CYAN 3
#define IO_RED 4
#define IO_MAGENTA 5
#define IO_BROWN 6
#define IO_LIGHT_GREY 7
#define IO_DARK_GREY 8
#define IO_LIGHT_BLUE 9
#define IO_LIGHT_GREEN 10
#define IO_LIGHT_CYAN 11
#define IO_LIGHT_RED 12
#define IO_LIGHT_MAGENTA 13
#define IO_LIGHT_BROWN 14
#define IO_WHITE 15


/* Clear the screen */
void io_clear();

/* Write n byte from the buffer */
int io_write(const char *buffer, unsigned n);

/* Write a byte from the buffer */
void io_write_char(char c);

/* Write the null terminated string */
int io_print(const char *buffer);

/* The bg_color and fg_color must be between 0 and 15 */
void io_write_frame(u16 pos, char c, u8 bg_color, u8 fg_color);

/* Move the cursor to the given pos */
void io_move_cursor(u16 pos);

#endif /* IO_H */
