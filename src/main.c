#include "io.h"
#include "pic.h"
#include "logger.h"
#include "interrupt.h"
#include "segmentation.h"

void start(void)
{
    io_clear();

    seg_flat_setup();
    io_print("Init the GDT ... Done !\n");

    pic_init();
    io_print("Remap the PIC ... Done !\n");

    idt_init();
    io_print("Loading IDT ... Done !\n");

}

typedef void (*call_module_t)(void);

int kmain(void)
{
    io_print("\n"
"                                   .-'\"`/\\\n"
"                                  // /' /\\`\\\n"
"                                ('//.-'/`-.;\n"
"                                 \\ \\ / /-.\n"
"              __.__.___..__._.___.\\\\ \\\\----,_\n"
"           .:{@&#,&#@&,@&#&&,#&@#&@&\\\\` \\-. .-'-.\n"
"        .:{@#@,#@&#,@#&&#,@&#&@&,&@#&&\\\\, -._,\"- \\\n"
"      .{#@#&@#@#&#&@&#@#@&#,@#@#&@&&#@#\\ \\// = \\`=\\__\n"
"      `{#@,@#&@&,@&#@,#@&#@#&@#&@,&#@,#/\\/ =`-. -_=__\n"
"        `:{@#&@&#@&#@&#@,#&&#@&,@#/.'  / / \"/.-', /\n"
"   ______  `:{@#&,#&@#,@&#&@&,@&#/.-// //-'-_= \",/\n"
"  / _____|    `~`~~`~~~`~`~`~~`~( / , /__,___.-\"\n"
" |  |     ___  _ __ ____         \\ \\\\/\n"
" |  |    / _ \\| '__|  _ \\         `\\\\\\'\n"
" |  |___| ( ) | |  | | | |\n"
"  \\______\\___/|_|  |_| |_|\n\n");

    log_info("hello");
    log_warn("hello not so urgent..");
    log_err("hello panic !");
    io_print("Messages sent !\n");

    for (;;)
        continue;

    return 0;
}
