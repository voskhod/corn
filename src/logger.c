#include "types.h"
#include "serial.h"

static void send_message(const char *msg);

void log_info(const char *msg)
{
    if (msg == NULL)
        return;

    send_message("[ INFO  ] ");
    send_message(msg);
    send_message("\n");
}

void log_warn(const char *msg)
{
    if (msg == NULL)
        return;

    send_message("[WARNING] ");
    send_message(msg);
    send_message("\n");
}

void log_err(const char *msg)
{
    if (msg == NULL)
        return;

    send_message("[ ERROR ] ");
    send_message(msg);
    send_message("\n");
}


static void send_message(const char *msg)
{
    static char is_com1_init = FALSE;
    if (!is_com1_init)
    {
        serial_init(COM1);
        is_com1_init = TRUE;
    }

    for (; *msg != '\0'; msg++)
        serial_write(COM1, *msg);
}
