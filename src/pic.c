#include "pic.h"
#include "wrapper.h"



void pic_init()
{
    /* Initialisation de ICW1 */
    outb(0x20, 0x11);
    outb(0xA0, 0x11);

    /* Initialisation de ICW2 */
    outb(0x21, 0x20);
    outb(0xA1, 0x28);

    /* Initialisation de ICW3 */
    outb(0x21, 0x04);
    outb(0xA1, 0x02);

    /* Initialisation de ICW4 */
    outb(0x21, 0x01);
    outb(0xA1, 0x01);

    /* masquage des interruptions */
    outb(0x21, 0x0);
    outb(0xA1, 0x0);
}


void pic_mask(u16 port)
{
    u16 pic = PIC1_DATA;

    if (port >= 8)
    {
        pic = PIC2_DATA;
        port -= 8;
    }

    outb(pic, inb(pic) | (1 << port));
}


void pic_ack(unsigned interrupt)
{
    /* If the interrupt came from the slave, we send ack */
    if (interrupt >= 8)
        outb(PIC2_CMD, PIC_ACK);

    /* Always ack master */
    outb(PIC1_CMD, PIC_ACK);
}
