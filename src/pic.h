#ifndef PIC_H
#define PIC_H

#include "types.h"

#define PIC1_CMD 0x20
#define PIC2_CMD 0xA0
#define PIC1_DATA 0x21
#define PIC2_DATA 0xA1

#define PIC1_START_INTERRUPT 0x20
#define PIC2_START_INTERRUPT 0x28
#define PIC2_END_INTERRUPT PIC2_START_INTERRUPT + 7

#define PIC_ACK 0x20

void pic_init();
//void pic_mask(u16 port);
//void pic_remap();
void pic_ack(unsigned interrup);


#endif /* PIC_H */
