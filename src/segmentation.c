#include "segmentation.h"
#include "wrapper.h"
#include "types.h"

#define GDT_BASE 0x0
#define GDT_SIZE 0xFF

struct gdt_desc
{
    u16 lim0_15;
    u16 base0_15;
    u8 base16_23;
    u8 acces;
    u8 lim16_19:4;
    u8 other:4;
    u8 base24_31;
}__attribute__((packed));


static struct gdt_desc gdt_entries[GDT_SIZE];

void init_gdt_desc(u32 base, u32 limit, u8 access, u8 other, struct gdt_desc *desc);

void seg_flat_setup()
{
    /* Null */
    init_gdt_desc(0x0, 0x0, 0x0, 0x0, &gdt_entries[0]);
    /* Code */
    init_gdt_desc(0x0, 0xFFFFF, 0x9B, 0x0D, &gdt_entries[1]);
    /* Data */
    init_gdt_desc(0x0, 0xFFFFF, 0x93, 0x0D, &gdt_entries[2]);

    struct gdt gdt = { .limit=sizeof(gdt_entries), .base=gdt_entries };
    load_gdt(&gdt);
    load_seg();
}


void init_gdt_desc(u32 base, u32 limit, u8 access, u8 other, struct gdt_desc *desc)
{
    desc->lim0_15 = (limit & 0xffff);
    desc->base0_15 = (base & 0xffff);
    desc->base16_23 = (base & 0xff0000) >> 16;
    desc->acces = access;
    desc->lim16_19 = (limit & 0xf0000) >> 16;
    desc->other = (other & 0xf);
    desc->base24_31 = (base & 0xff000000) >> 24;
}
