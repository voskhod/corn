#ifndef INTERRUPT_H
#define INTERRUPT_H

#include "types.h"


#define IDT_SIZE 256

#define KEYBOARD_PORT 0x60

struct interrupt_frame
{
    int errCode;
    int eip;
    int ecs;
    int flags;
}__attribute__((packed));


struct idt_entry{
        u16 offset_lowerbits;
        u16 selector;
        u8 zero;
        u8 type_attr;
        u16 offset_higherbits;
}__attribute__((packed));


void idt_init(void);


/* Handlers */
typedef void(* handler_t)(void *);
void default_handler(void *ptr);
void default_handler2(void *ptr);
void keyboard_handler(void *ptr);



#endif /* INTERRUPT_H */
