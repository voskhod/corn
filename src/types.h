#ifndef TYPES_H
#define TYPES_H

#define NULL ((void *)0)

#define TRUE 1
#define FALSE 0

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

#endif /* TYPES_H */
