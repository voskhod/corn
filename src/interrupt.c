#include "io.h"
#include "pic.h"
#include "wrapper.h"
#include "interrupt.h"
#include "scan_code.h"

struct idt_entry idt[IDT_SIZE];


void idt_set_entry(handler_t handler, struct idt_entry *entry)
{
    u32 address = (u32)handler; 
    entry->offset_lowerbits = address & 0xffff;
    entry->offset_higherbits = (address & 0xffff0000) >> 16;

    /* KERNEL_CODE_SEGMENT_OFFSET */
    entry->selector = 0x08;
    entry->zero = 0;

    /* INTERRUPT_GATE */
    entry->type_attr = 0x8e;
}


void idt_init(void) {
    for (unsigned i = 0; i < 7; i++)
        idt_set_entry(default_handler, &idt[32 + i]);
    for (unsigned i = 7; i < 16; i++)
        idt_set_entry(default_handler2, &idt[32 + i]);
    idt_set_entry(keyboard_handler, &idt[32 + 1]);

    /* fill the idt descriptor */
    u32 idt_address = (unsigned long)idt ;
    u32 size = IDT_SIZE * sizeof(struct idt_entry);

    struct idt_register idtr =
    {
        .limit=size + ((idt_address & 0xffff) << 16),
        .base=idt_address >> 16
    };
    load_idt(&idtr);
}


__attribute__((interrupt))
void default_handler(void *ptr) {
    (void)ptr;
    outb(0x20, 0x20); //EOI
}

__attribute__((interrupt))
void default_handler2(void *ptr) {
    (void)ptr;
    outb(0xA0, 0x20);
    outb(0x20, 0x20); //EOI
}


__attribute__((interrupt))
void keyboard_handler(void *ptr)
{
    (void)ptr;
    unsigned char c;
    static u8 shift_enable;
    static u8 alt_enable;
    static u8 ctrl_enable;
    (void)alt_enable;
    (void)ctrl_enable;

    c = inb(KEYBOARD_PORT);

    /* Touch pressed */
    if (c < 0x80)
    {
        if (c == 0x2A || c == 0x36)
            shift_enable = 1;
        else if (c == 0x1D)
            ctrl_enable = 1;
        else if (c == 0x38)
            alt_enable = 1;
        else
        {
            /* Convert and print char */
            c = asccode[c][shift_enable];
            io_write_char(c);
        }
    }
    else
    {
        c -= 0x80;
        if (c == 0x2A || c == 0x36)
            shift_enable = 0;
        else if (c == 0x1D)
            ctrl_enable = 0;
        else if (c == 0x38)
            alt_enable = 0;
    }

    pic_ack(1);
}
