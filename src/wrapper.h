#ifndef WRAPPER_H
#define WRAPPER_H

#include "types.h"

/* IO Ports */
#define IO_PORT_COMMAND 0x3D4
#define IO_PORT_DATA 0x3D5

/* IO command */
#define IO_COMMAND_BYTE_SUP 14
#define IO_COMMAND_BYTE_INF 15


struct idt_register
{
    u32 limit;
    u32 base;
};

struct gdt
{
    u16 limit;
    void *base;
} __attribute__((packed));



/* Send some data to the IO port */
void outb(u16 port, unsigned char data);

/* Read an octed from the IO port */
u8 inb(u16 port);

/* Load the gdt table */
void load_gdt(struct gdt *gdt);

/* Load the segment into the registre */
void load_seg();

/* Wait for io */
void load_idt(struct idt_register *idt);

#endif /* WRAPPER_H */
