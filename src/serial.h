#ifndef SERIAL_H
#define SERIAL_H

#include "types.h"

#define COM1 0x3F8


/* Init the serial port */
void serial_init(u16 com);

/* Write a char on the given serial port */
void serial_write(u16 com, char c);

#endif /* SERIAL_H */
