#ifndef LOGGER_H
#define LOGGER_H

/* Send messages on COM1 */
void log_info(const char *msg);
void log_warn(const char *msg);
void log_err(const char *msg);

#endif /* LOGGER_H */
