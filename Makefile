CC=gcc
CFLAGS=-Wall -Wextra -pedantic -std=c11 -o0 -g \
        -mgeneral-regs-only -mno-red-zone -mgeneral-regs-only \
        -m32 -nostdlib -nostdinc -fno-builtin -fno-stack-protector -nostartfiles  -nodefaultlibs

AS=gcc
ASFLAGS= -m32 -nostdlib -nostdinc -fno-builtin -fno-stack-protector -nostartfiles  -nodefaultlibs


LDFLAGS=-T src/link.ld -melf_i386

VPATH=src
OBJ=$(OBJ_MAIN) $(OBJ_IO) $(OBJ_UTILS)
OBJ_MAIN=loader.o main.o
OBJ_IO=io.o serial.o logger.o
OBJ_UTILS=wrapper.o segmentation.o interrupt.o interrupt_wrap.o pic.o

.PHONY: all
all: os.iso

os.iso: kernel
	mkdir -p iso/boot/grub
	cp grub/* iso/boot/grub/
	cp kernel iso/boot/
	genisoimage -R                              \
            -b boot/grub/stage2_eltorito    \
            -no-emul-boot                   \
            -boot-load-size 4               \
            -A os                           \
            -input-charset utf8             \
            -quiet                          \
            -boot-info-table                \
            -o os.iso                       \
            iso


%.o: %.S
	$(AS) $(ASFLAGS) -c $< -o $@

kernel: $(OBJ)
	$(LD) $(LDFLAGS) $^ -o $@

run: os.iso
	bochs -f bochsrc.txt -q

.PHONY: clean
clean:
	rm -f $(OBJ) kernel
	rm -rf iso os.iso
	rm -f bochslog.txt *.out
